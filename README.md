# README #

Siéntase libre de clonar y hacer pull requests sobre este repositorio

## ¿Para qué este repositorio? ##

Aquí tenemos los archivos de los temas para moinmoin del flisol

## ¿Cómo configuro? ##

Obtenga una versión de moinmoin y configúrelo para que tenga el tema en
cuestión, de esta forma podrá probar localmente.

```
wget http://static.moinmo.in/files/moin-1.9.4.tar.gz
tar xfz moin-1.9.4.tar.gz
cd moin-1.9.4
```

Esto le permitirá contar con moinmoin en la versión que está corriendo la
instancia en producción.

En este punto asumimos que usted ya ha clonado el repositorio y tiene el
directorio del respositorio configurado en la variable `REPO_HOME`.


```
cp $REPO_HOME/sol/sol.py MoinMoin/theme/
cp -r $REPO_HOME/sol/sol MoinMoin/web/static/htdocs/
cp $REPO_HOME/conf/wiki_flisol.py wikiconfig.py
./wikiserver.py
```

Si visita con un navegador http://localhost:8080 podrá ver el resultado

### La barra lateral y la página principal ###

La barra lateral se configura gracias a la página SideBar, puede visitar
http://wiki.installfest.info/SideBar/?action=raw y copiar los contenidos en
su instancia local excluyendo la línea #acl inicial vía
http://localhost:8080/SideBar/?action=edit

Así mismo, puede obtener la página home, en este caso deseará visitar
http://wiki.installfest.info/?action=raw y copiar y pegar los contenidos en
http://wiki.installfest.info/?action=edit

En ambos casos tenga en cuenta que las imágenes están configuradas como
adjuntos.

## ¿Cómo contribuir? ##

* Reportar fallos
* Clonar y hacer pull request del repositorio
* Ofrecer versión responsive
* Quiere hacerlo con less o sass para generar el css? adelante :)

## Quiero comunicarme con alguien ¿cómo hago? ##

* Todo se maneja a través de la lista de coordinación del Flisol en temas
técnicos.
